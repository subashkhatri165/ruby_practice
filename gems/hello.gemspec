Gem::Specification.new do |s|
    s.name        = 'hello'
    s.version     = '0.0.1'
    s.date        = '2019-09-16'
    s.summary     = "Hello!"
    s.description = "A simple hello world gem"
    s.authors     = ["Subash Khatri"]
    s.email       = 'subashkhatri165@gmail.com'
    s.files       = ["lib/hello.rb"]
    s.homepage    =
      'https://rubygems.org/gems/hello'
    s.license       = 'MIT'
  end