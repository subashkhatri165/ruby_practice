#These methods are the methods that can be called from an instance

class InstanceMethod
    def put_value
    puts "Good Evening! "
    end

end
obj = InstanceMethod.new
obj.put_value

#class method
## These are static methods, i.e they can be invoked on the class and not on instatiation
#of the class
#it is equivalent to use "self" in place of the class name

# class StaticMethod
#     def self.hello(name)
#     puts "hello, #{name}"
#     end
# end
# StaticMethod.hello("subash-khatri")

##Singleton Method
## These are only available to speceific instances of the class, but
## not to all

# class PrintValue
#     #empty class
# end
# #instances of the class
# thing1 = PrintValue.new
# thing2 = PrintValue.new
# #create a singleton method
# def thing1.values_in
# puts "hello everyone in this world"
# end

# #printing the value from method

# thing1.values_in
# thing2.values_in ## this gives an error of 
#####This is similar to above execution but in advance form
class Thing
    class << self
        def hello(name)
        puts "hello, #{name}!"

        end
    end
end
Thing.hello("subashkhatri")