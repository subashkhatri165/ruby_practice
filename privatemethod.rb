class PrivateName
    attr_accessor = :name
    def initialize(name)
        @name = name
    end   
    def define_name
        age = define_age
        puts "I'm #{@name} and I am #{age} years old "
    end 
    private
    def define_age
       88-64
    end
end
nam = PrivateName.new("subash_khatri")
nam.define_name
# nam.define_age #This will be method error..it cannot be called outside of the function
