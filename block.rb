# def test

#     puts " Start the code here"
#     yield
#     yield
    
# end 
# test { puts "you are on the block side"}

# def reset
#     puts "Second line of the code is here"
#     yield
# end
# # reset { puts " for reset line"}
# def testing_block_yield
#     yield 5
#     puts "now it sends another value"
#     yield 10
# end

# testing_block_yield{|i| puts "The Value is #{i}"}

def test(&block)
    block.call
end
test{ puts "Data is important component"}