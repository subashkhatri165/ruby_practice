class ProctedName
     attr_accessor :name, :age
    def initialize(name, age)
    @name = name
    @age = age

    end
    
    def define_name
    puts "I'm #{@name} and I 'm #{@age} years old"
    end

    def ==(other)
        self.own_age == other.own_age
    end

    protected
    def own_age
        self.age
    end
end
nam1 = ProctedName.new("hello", 2)
nam2 = ProctedName.new("hello2", 4)
name3 = ProctedName.new("hello3", 2)
puts nam1.name
puts nam1.age

