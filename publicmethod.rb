#Public Method
## Public Method Can be acced from out side of the scope of the created object

##It Can be inizilied by the Public key word but in ruby it is not needed

class NameService
    attr_accessor :name
    def initialize(name)
        @name = name.capitalize
    end
    def specify_name 
        puts "My name is #{@name} and I ' love being called #{@name}"
    end

end
name_finalize = NameService.new("subash-khatri")
name_finalize.specify_name
